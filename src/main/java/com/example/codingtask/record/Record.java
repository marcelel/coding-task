package com.example.codingtask.record;

import com.google.common.base.Preconditions;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;

final class Record {

    private final RecordPrimaryKey primaryKey;
    private final String name;
    private final String description;
    private final LocalDateTime updatedTimestamp;

    Record(RecordPrimaryKey primaryKey, String name, String description, LocalDateTime updatedTimestamp) {
        Preconditions.checkArgument(primaryKey != null, "Primary key can't be null");

        this.primaryKey = primaryKey;
        this.name = name;
        this.description = description;
        this.updatedTimestamp = updatedTimestamp;
    }

    RecordPrimaryKey primaryKey() {
        return primaryKey;
    }

    Optional<String> name() {
        return Optional.ofNullable(name);
    }

    Optional<String> description() {
        return Optional.ofNullable(description);
    }

    Optional<LocalDateTime> updatedTimestamp() {
        return Optional.ofNullable(updatedTimestamp);
    }

    RecordView createView() {
        return new RecordView(primaryKey.value(), name, description, updatedTimestamp);
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        Record record = (Record) other;
        return primaryKey.equals(record.primaryKey);
    }

    @Override
    public int hashCode() {
        return Objects.hash(primaryKey);
    }

    @Override
    public String toString() {
        return "Record{primaryKey='" + primaryKey.value() + '\'' + '}';
    }
}
