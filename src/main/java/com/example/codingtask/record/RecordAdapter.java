package com.example.codingtask.record;

import com.google.common.base.Preconditions;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;

final class RecordAdapter {

    private static final String FIRST_LINE = "header: PRIMARY_KEY,NAME,DESCRIPTION,UPDATED_TIMESTAMP";

    private RecordAdapter() {
    }

    static List<Record> adapt(String file) {
        Preconditions.checkArgument(StringUtils.isNotBlank(file), "File can't be blank");

        List<String> lines = file.lines()
                                 .collect(Collectors.toList());
        validateFirstLine(lines);
        validateLastLine(lines);

        return lines.subList(1, lines.size() - 1)
                    .stream()
                    .map(RecordAdapter::adaptSingleRecord)
                    .collect(Collectors.toList());
    }

    private static void validateFirstLine(List<String> lines) {
        String firstLine = lines.get(0);
        if (!firstLine.equals(FIRST_LINE)) {
            throw new IllegalArgumentException("Invalid first line of file");
        }
    }

    private static void validateLastLine(List<String> lines) {
        String lastLine = lines.get(lines.size() - 1);
        if (!lastLine.isEmpty()) {
            throw new IllegalArgumentException("Invalid last line of file");
        }
    }

    private static Record adaptSingleRecord(String line) {
        String[] fields = line.split(",");
        if (fields.length != 4) {
            throw new IllegalArgumentException("Invalid format of file");
        }

        RecordPrimaryKey primaryKey = new RecordPrimaryKey(fields[0]);
        String name = fields[1];
        String description = fields[2];
        LocalDateTime updatedTimestamp = StringUtils.isNotBlank(fields[3]) ? parseTimestamp(fields[3]) : null;
        return new Record(primaryKey, name, description, updatedTimestamp);
    }

    private static LocalDateTime parseTimestamp(String timestamp) {
        try {
            return LocalDateTime.parse(timestamp);
        } catch (Exception e) {
            throw new IllegalArgumentException("Timestamp in invalid format");
        }
    }
}
