package com.example.codingtask.record;

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/records")
@RestController
final class RecordController {

    private final RecordService recordService;

    RecordController(RecordService recordService) {
        this.recordService = recordService;
    }

    @PostMapping
    HttpStatus createRecords(@RequestBody String file) {
        Preconditions.checkArgument(StringUtils.isNotBlank(file), "File can't be blank");

        recordService.createRecords(file);
        return HttpStatus.OK;
    }

    @GetMapping("/{primaryKey}")
    ResponseEntity<RecordView> findRecord(@PathVariable String primaryKey) {
        Record record = recordService.findRecord(new RecordPrimaryKey(primaryKey));
        return ResponseEntity.ok(record.createView());
    }

    @DeleteMapping("/{primaryKey}")
    HttpStatus deleteRecord(@PathVariable String primaryKey) {
        recordService.deleteRecord(new RecordPrimaryKey(primaryKey));
        return HttpStatus.OK;
    }
}
