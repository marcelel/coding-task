package com.example.codingtask.record;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.DataStorageConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.springframework.stereotype.Repository;

@Repository
class RecordIgniteRepository implements RecordRepository {

    private final IgniteCache<RecordPrimaryKey, Record> cache = initCache();

    @Override
    public void saveAll(List<Record> records) {
        Map<RecordPrimaryKey, Record> map = records.stream()
                                                   .collect(Collectors.toMap(Record::primaryKey, Function.identity()));
        cache.putAll(map);
    }

    @Override
    public Optional<Record> findById(RecordPrimaryKey primaryKey) {
        return Optional.ofNullable(cache.get(primaryKey));
    }

    @Override
    public void delete(Record record) {
        boolean removed = cache.remove(record.primaryKey());
        if (!removed) {
            throw new IllegalArgumentException("Record with " + record.primaryKey().value() + " does not exist");
        }
    }

    private IgniteCache<RecordPrimaryKey, Record> initCache() {
        IgniteConfiguration igniteConfiguration = new IgniteConfiguration();
        DataStorageConfiguration dataStorageConfiguration = new DataStorageConfiguration();
        dataStorageConfiguration.getDefaultDataRegionConfiguration().setPersistenceEnabled(true);
        igniteConfiguration.setDataStorageConfiguration(dataStorageConfiguration);
        Ignite ignite = Ignition.start(igniteConfiguration);
        ignite.cluster().active(true);
        return ignite.getOrCreateCache("records");
    }
}
