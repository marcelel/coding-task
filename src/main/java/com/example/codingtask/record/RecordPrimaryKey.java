package com.example.codingtask.record;

import com.google.common.base.Preconditions;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;

final class RecordPrimaryKey {

    private final String value;

    RecordPrimaryKey(String value) {
        Preconditions.checkArgument(StringUtils.isNotBlank(value), "Record primary key can't be blank");

        this.value = value;
    }

    String value() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RecordPrimaryKey that = (RecordPrimaryKey) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return "RecordPrimaryKey{value='" + value + '\'' + '}';
    }
}
