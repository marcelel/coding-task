package com.example.codingtask.record;

import java.util.List;
import java.util.Optional;

interface RecordRepository {

    void saveAll(List<Record> records);

    Optional<Record> findById(RecordPrimaryKey primaryKey);

    void delete(Record record);
}
