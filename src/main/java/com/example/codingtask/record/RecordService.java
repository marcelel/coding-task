package com.example.codingtask.record;

import java.util.List;
import org.springframework.stereotype.Service;

@Service
final class RecordService {

    private final RecordRepository recordRepository;

    RecordService(RecordRepository recordRepository) {
        this.recordRepository = recordRepository;
    }

    void createRecords(String file) {
        List<Record> records = RecordAdapter.adapt(file);
        recordRepository.saveAll(records);
    }

    Record findRecord(RecordPrimaryKey primaryKey) {
        return recordRepository.findById(primaryKey)
                               .orElseThrow(() -> new IllegalArgumentException("Record with " + primaryKey.value() + " does not exist"));
    }

    void deleteRecord(RecordPrimaryKey primaryKey) {
        Record record = findRecord(primaryKey);
        recordRepository.delete(record);
    }
}
