package com.example.codingtask.record;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import java.time.LocalDateTime;

public final class RecordView {

    public final String primaryKey;
    public final String name;
    public final String description;
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    public final LocalDateTime updatedTimestamp;

    public RecordView(String primaryKey, String name, String description, LocalDateTime updatedTimestamp) {
        this.primaryKey = primaryKey;
        this.name = name;
        this.description = description;
        this.updatedTimestamp = updatedTimestamp;
    }
}
