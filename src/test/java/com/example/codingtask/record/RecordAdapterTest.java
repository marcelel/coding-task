package com.example.codingtask.record;

import java.time.LocalDateTime;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RecordAdapterTest {

    @Test
    void adapt_fileIsNull_expectIllegalArgumentException() {
        String file = null;

        Assertions.assertThatThrownBy(() -> RecordAdapter.adapt(file))
                  .isInstanceOf(IllegalArgumentException.class)
                  .hasMessage("File can't be blank");
    }

    @Test
    void adapt_fileIsEmpty_expectIllegalArgumentException() {
        String file = "";

        Assertions.assertThatThrownBy(() -> RecordAdapter.adapt(file))
                  .isInstanceOf(IllegalArgumentException.class)
                  .hasMessage("File can't be blank");
    }

    @Test
    void adapt_fileIsBlank_expectIllegalArgumentException() {
        String file = " ";

        Assertions.assertThatThrownBy(() -> RecordAdapter.adapt(file))
                  .isInstanceOf(IllegalArgumentException.class)
                  .hasMessage("File can't be blank");
    }

    @Test
    void adapt_fileWithoutValidFirstLine_expectIllegalArgumentException() {
        String file = "Invalid file";

        Assertions.assertThatThrownBy(() -> RecordAdapter.adapt(file))
                  .isInstanceOf(IllegalArgumentException.class)
                  .hasMessage("Invalid first line of file");
    }

    @Test
    void adapt_fileWithoutValidLastLine_expectIllegalArgumentException() {
        String file = "header: PRIMARY_KEY,NAME,DESCRIPTION,UPDATED_TIMESTAMP\n";

        Assertions.assertThatThrownBy(() -> RecordAdapter.adapt(file))
                  .isInstanceOf(IllegalArgumentException.class)
                  .hasMessage("Invalid last line of file");
    }

    @Test
    void adapt_fileWithInvalidInnerLine_expectIllegalArgumentException() {
        String file = "header: PRIMARY_KEY,NAME,DESCRIPTION,UPDATED_TIMESTAMP\n"
                + "invalid inner line\n"
                + "\n";

        Assertions.assertThatThrownBy(() -> RecordAdapter.adapt(file))
                  .isInstanceOf(IllegalArgumentException.class)
                  .hasMessage("Invalid format of file");
    }

    @Test
    void adapt_fileWithInvalidTimestampInInnerLine_expectIllegalArgumentException() {
        String file = "header: PRIMARY_KEY,NAME,DESCRIPTION,UPDATED_TIMESTAMP\n"
                + "1,name1,desc1,2020-08:55:37\n"
                + "\n";

        Assertions.assertThatThrownBy(() -> RecordAdapter.adapt(file))
                  .isInstanceOf(IllegalArgumentException.class)
                  .hasMessage("Timestamp in invalid format");
    }

    @Test
    void adapt_fileWithoutInnerLines_expectEmptyList() {
        String file = "header: PRIMARY_KEY,NAME,DESCRIPTION,UPDATED_TIMESTAMP\n\n";

        List<Record> records = RecordAdapter.adapt(file);

        assertNotNull(records);
        assertTrue(records.isEmpty());
    }

    @Test
    void adapt_fileWithValidInnerLines_expectRecords() {
        String file = "header: PRIMARY_KEY,NAME,DESCRIPTION,UPDATED_TIMESTAMP\n"
                + "1,name1,desc1,2020-08-02T21:55:37\n"
                + "2,name2,desc2,2020-08-02T20:55:37\n"
                + "\n";
        Record record1 = new Record(new RecordPrimaryKey("1"), "name1", "desc1", LocalDateTime.parse("2020-08-02T21:55:37"));
        Record record2 = new Record(new RecordPrimaryKey("2"), "name2", "desc2", LocalDateTime.parse("2020-08-02T20:55:37"));

        List<Record> records = RecordAdapter.adapt(file);

        assertNotNull(records);
        assertEquals(2, records.size());
        assertEquals(record1, records.get(0));
        assertEquals(record2, records.get(1));
    }
}
