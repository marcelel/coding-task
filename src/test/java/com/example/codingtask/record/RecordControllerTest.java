package com.example.codingtask.record;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class RecordControllerTest {

    private final ObjectMapper objectMapper = Jackson2ObjectMapperBuilder.json()
                                                                         .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                                                                         .serializerByType(LocalDateTime.class,
                                                                                 new LocalDateTimeSerializer(DateTimeFormatter.BASIC_ISO_DATE))
                                                                         .build();

    @MockBean
    private RecordRepository repository;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void createRecords_fileIsInvalid_expectBadRequest() throws Exception {
        String file = "invalid";

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/records")
                                                                            .contentType(MediaType.TEXT_PLAIN)
                                                                            .content(file));

        verify(repository, never()).saveAll(anyList());
        resultActions.andExpect(status().isBadRequest())
                     .andExpect(content().string(Matchers.containsString("Invalid first line of file")));
    }

    @Test
    void createRecords_fileIsValid_expectOk() throws Exception {
        String file = "header: PRIMARY_KEY,NAME,DESCRIPTION,UPDATED_TIMESTAMP\n"
                + "1,name1,desc1,2020-08-02T21:55:37\n"
                + "\n";
        Record record = new Record(new RecordPrimaryKey("1"), "name1", "desc1", LocalDateTime.parse("2020-08-02T21:55:37"));
        List<Record> records = Collections.singletonList(record);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/records")
                                                                            .contentType(MediaType.TEXT_PLAIN)
                                                                            .content(file));

        verify(repository).saveAll(eq(records));
        resultActions.andExpect(status().isOk());
    }

    @Test
    void findRecord_recordDoesNotExist_expectBadRequest() throws Exception {
        RecordPrimaryKey primaryKey = new RecordPrimaryKey("1");
        when(repository.findById(eq(primaryKey))).thenReturn(Optional.empty());

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/records/1"));

        resultActions.andExpect(status().isBadRequest())
                     .andExpect(content().string(Matchers.containsString("Record with 1 does not exist")));
    }

    @Test
    void findRecord_recordExists_expectOk() throws Exception {
        RecordPrimaryKey primaryKey = new RecordPrimaryKey("1");
        Record record = new Record(primaryKey, "name1", "desc1", LocalDateTime.parse("2020-08-02T21:55:37"));
        when(repository.findById(eq(primaryKey))).thenReturn(Optional.of(record));

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/records/1"));

        resultActions.andExpect(status().isOk())
                     .andExpect(content().json(objectMapper.writeValueAsString(record.createView())));
    }

    @Test
    void deleteRecord_recordDoesNotExist_expectBadRequest() throws Exception {
        RecordPrimaryKey primaryKey = new RecordPrimaryKey("1");
        when(repository.findById(eq(primaryKey))).thenReturn(Optional.empty());

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.delete("/records/1"));

        resultActions.andExpect(status().isBadRequest())
                     .andExpect(content().string(Matchers.containsString("Record with 1 does not exist")));
    }

    @Test
    void deleteRecord_recordExists_expectOk() throws Exception {
        RecordPrimaryKey primaryKey = new RecordPrimaryKey("1");
        Record record = new Record(primaryKey, "name1", "desc1", LocalDateTime.parse("2020-08-02T21:55:37"));
        when(repository.findById(eq(primaryKey))).thenReturn(Optional.of(record));

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.delete("/records/1"));

        verify(repository).delete(eq(record));
        resultActions.andExpect(status().isOk());
    }
}
