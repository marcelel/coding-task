package com.example.codingtask.record;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RecordPrimaryKeyTest {

    @Test
    void new_valueIsNull_expectIllegalArgumentException() {
        String primaryKey = null;

        Assertions.assertThatThrownBy(() -> new RecordPrimaryKey(primaryKey))
                  .isInstanceOf(IllegalArgumentException.class)
                  .hasMessage("Record primary key can't be blank");
    }

    @Test
    void new_valueIsEmpty_expectIllegalArgumentException() {
        String primaryKey = "";

        Assertions.assertThatThrownBy(() -> new RecordPrimaryKey(primaryKey))
                  .isInstanceOf(IllegalArgumentException.class)
                  .hasMessage("Record primary key can't be blank");
    }

    @Test
    void new_valueIsBlank_expectIllegalArgumentException() {
        String primaryKey = " ";

        Assertions.assertThatThrownBy(() -> new RecordPrimaryKey(primaryKey))
                  .isInstanceOf(IllegalArgumentException.class)
                  .hasMessage("Record primary key can't be blank");
    }

    @Test
    void new_valueIsPresent_expectValidObject() {
        String primaryKey = "1";

        RecordPrimaryKey recordPrimaryKey = new RecordPrimaryKey(primaryKey);

        assertEquals(primaryKey, recordPrimaryKey.value());
    }

    @Test
    void value_expectValue() {
        String primaryKey = "1";
        RecordPrimaryKey recordPrimaryKey = new RecordPrimaryKey(primaryKey);

        String result = recordPrimaryKey.value();

        assertEquals(primaryKey, result);
    }
}
