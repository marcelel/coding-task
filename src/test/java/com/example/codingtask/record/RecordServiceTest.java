package com.example.codingtask.record;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class RecordServiceTest {

    private RecordService service;
    private RecordRepository repository;

    @BeforeEach
    void setUp() {
        repository = mock(RecordRepository.class);
        service = new RecordService(repository);
    }

    @Test
    void createRecords_fileIsInvalid_expectIllegalArgumentException() {
        String file = "invalid";

        Assertions.assertThatThrownBy(() -> service.createRecords(file))
                  .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void createRecords_fileIsValid_expectRecordsSaved() {
        String file = "header: PRIMARY_KEY,NAME,DESCRIPTION,UPDATED_TIMESTAMP\n"
                + "1,name1,desc1,2020-08-02T21:55:37\n"
                + "\n";
        Record record = new Record(new RecordPrimaryKey("1"), "name1", "desc1", LocalDateTime.parse("2020-08-02T21:55:37"));
        List<Record> records = Collections.singletonList(record);

        service.createRecords(file);

        verify(repository).saveAll(eq(records));
    }

    @Test
    void findRecord_recordDoesNotExist_expectIllegalArgumentException() {
        RecordPrimaryKey primaryKey = new RecordPrimaryKey("1");
        when(repository.findById(eq(primaryKey))).thenReturn(Optional.empty());

        Assertions.assertThatThrownBy(() -> service.findRecord(primaryKey))
                  .isInstanceOf(IllegalArgumentException.class)
                  .hasMessage("Record with 1 does not exist");
    }

    @Test
    void findRecord_recordExists_expectRecord() {
        RecordPrimaryKey primaryKey = new RecordPrimaryKey("1");
        Record record = new Record(primaryKey, "name1", "desc1", LocalDateTime.parse("2020-08-02T21:55:37"));
        when(repository.findById(eq(primaryKey))).thenReturn(Optional.of(record));

        Record result = service.findRecord(primaryKey);

        assertEquals(record, result);
    }

    @Test
    void deleteRecord_recordDoesNotExist_expectIllegalArgumentException() {
        RecordPrimaryKey primaryKey = new RecordPrimaryKey("1");
        when(repository.findById(eq(primaryKey))).thenReturn(Optional.empty());

        Assertions.assertThatThrownBy(() -> service.deleteRecord(primaryKey))
                  .isInstanceOf(IllegalArgumentException.class)
                  .hasMessage("Record with 1 does not exist");
    }

    @Test
    void deleteRecord_recordExists_expectRecordDeleted() {
        RecordPrimaryKey primaryKey = new RecordPrimaryKey("1");
        Record record = new Record(primaryKey, "name1", "desc1", LocalDateTime.parse("2020-08-02T21:55:37"));
        when(repository.findById(eq(primaryKey))).thenReturn(Optional.of(record));

        service.deleteRecord(primaryKey);

        verify(repository).delete(eq(record));
    }
}
