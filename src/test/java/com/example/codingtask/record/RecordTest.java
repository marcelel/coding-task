package com.example.codingtask.record;

import java.time.LocalDateTime;
import java.util.Optional;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RecordTest {

    @Test
    void new_primaryKeyIsNull_expectIllegalArgumentException() {
        Assertions.assertThatThrownBy(() -> new Record(null, null, null, null))
                  .isInstanceOf(IllegalArgumentException.class)
                  .hasMessage("Primary key can't be null");
    }

    @Test
    void new_primaryKeyIsNotNull_expectValidObject() {
        Record record = new Record(new RecordPrimaryKey("1"), null, null, null);

        assertEquals("1", record.primaryKey().value());
    }

    @Test
    void primaryKey_expectEqualPrimaryKey() {
        RecordPrimaryKey primaryKey = new RecordPrimaryKey("1");
        Record record = new Record(primaryKey, null, null, null);

        assertEquals(primaryKey, record.primaryKey());
    }

    @Test
    void name_nameIsNull_expectEmpty() {
        RecordPrimaryKey primaryKey = new RecordPrimaryKey("1");
        Record record = new Record(primaryKey, null, null, null);

        assertEquals(Optional.empty(), record.name());
    }

    @Test
    void name_nameIsNotNull_expectValue() {
        RecordPrimaryKey primaryKey = new RecordPrimaryKey("1");
        Record record = new Record(primaryKey, "name", null, null);

        assertTrue(record.name().isPresent());
        assertEquals("name", record.name().get());
    }

    @Test
    void description_descriptionIsNull_expectEmpty() {
        RecordPrimaryKey primaryKey = new RecordPrimaryKey("1");
        Record record = new Record(primaryKey, null, null, null);

        assertEquals(Optional.empty(), record.description());
    }

    @Test
    void description_descriptionIsNotNull_expectValue() {
        RecordPrimaryKey primaryKey = new RecordPrimaryKey("1");
        Record record = new Record(primaryKey, null, "description", null);

        assertTrue(record.description().isPresent());
        assertEquals("description", record.description().get());
    }

    @Test
    void updatedTimestamp_updatedTimestampIsNull_expectEmpty() {
        RecordPrimaryKey primaryKey = new RecordPrimaryKey("1");
        Record record = new Record(primaryKey, null, null, null);

        assertEquals(Optional.empty(), record.updatedTimestamp());
    }

    @Test
    void updatedTimestamp_updatedTimestampIsNotNull_expectValue() {
        RecordPrimaryKey primaryKey = new RecordPrimaryKey("1");
        LocalDateTime updatedTimestamp = LocalDateTime.now();
        Record record = new Record(primaryKey, null, null, updatedTimestamp);

        assertTrue(record.updatedTimestamp().isPresent());
        assertEquals(updatedTimestamp, record.updatedTimestamp().get());
    }
}
